/*Assign JSON array to Javascript variable */
var indicatorText = '';
jQuery(document).ready(function($) {
var div1 = $('<div id="filterContainer" style="height:50px;border=1px solid ffffff"/>');
$('#edit-indicators').append(div1);
$("#filterContainer").append("<select id='lstIndicators'></select>");
//Creating divisions for different graph types 
var BARs = $('<div id="BARs" style="width: 900px; height: 500px;"/>').text("BAR");
var AREAs = $('<div id="AREAs" style="width: 900px; height: 500px;"/>').text("AREA");
var COLUMNs = $('<div id="COLUMNs" style="width: 900px; height: 500px;"/>').text("COLUMN");
var LINEs = $('<div id="LINEs" style="width: 900px; height: 500px;"/>').text("LINE");
var COMPOs = $('<div id="COMPOs" style="width: 900px; height: 500px;"/>').text("COMPO");
var PIEs = $('<div id="PIEs" style="width: 900px; height: 500px;"/>').text("PIE");
$('#edit-charts').append(BARs);
$('#edit-charts').append(AREAs);
$('#edit-charts').append(COLUMNs);
$('#edit-charts').append(LINEs);
$('#edit-charts').append(COMPOs);
$('#edit-charts').append(PIEs);
// Creating and populationg the chart type selection dropdown
$("#edit-charttype").append("<select id='lstcharttype'></select>");
var chartTypes=new Array('--All Type--','Bar','Area','Column','Line','Compo','Pie');
$.each(chartTypes, function(index, val){
	$('#lstcharttype').append( $('<option></option>').val(index).html(val));
});
$("#edit-charttype").append("<input type='button' id='btnGo' value='Display Chart' />");
//parsing the JSON array
data = jQuery.parseJSON(Drupal.settings.chartgeneration.jsonarr);
var data2 = data;
var newJSarray = [];
for (var i in data2){
  for(var x in data2[i]){
	  var dataVal = $.trim( data2[i][x].indicator_id + '|' + data2[i][x].indicator);
		newJSarray.push(dataVal); 
  } 
}
var newArray = newJSarray.filter(function(itm,i,newJSarray){
  return i==newJSarray.indexOf(itm);
});
$.each(newArray, function(index, val){
  var indicatorID = val.split('|');
	$('#lstIndicators').append( $('<option></option>').val(indicatorID[0]).html(indicatorID[1]) );
});
//Display all types of chart when page load.
var indicatorValuedefault = $('#lstIndicators').val();
indicatorText = $('#lstIndicators option:selected').text();
parseJsonArray(indicatorValuedefault);
//Display chart based on indiactor selection.
$('#btnGo').bind('click', function() {
  var indicatorValue = $('#lstIndicators').val();
  indicatorText = $('#lstIndicators option:selected').text();
  parseJsonArray(indicatorValue);
  var messages = jQuery.parseJSON(Drupal.settings.chartgeneration.jsonarr);
  $.each(messages, function(i, msg){
	   $.each(msg, function(key, message){
		   if (message.indicator_id === indicatorValue) {
		   }
		});
  });
});
/* Select values from JSON based on the selection */
function parseJsonArray(indicator) {
	var resultStr = '';
	var resultArr = new Array();
		resultArr['TITLE'] = ['Year'];
		var columncount = 0;
		for (var i in data) {
			 var country_data = data[i];
			 columncount = columncount+1
			 resultArr['TITLE'][columncount] = i;
			 for (var j in country_data) {
				 if (country_data[j].indicator_id == indicator) {
					 if (typeof (resultArr[country_data[j].year]) === 'undefined') {
						 resultArr[country_data[j].year] = [country_data[j].year];
					 }
					 if (country_data[j].value !='null') {
						 //alert(columncount);
						 if (resultArr[country_data[j].year][columncount] !='') {
							 resultArr[country_data[j].year][columncount] = country_data[j].value;
						 }
					 }
				 }
			 }
		}
	var g_data = '';
	g_data = new Array();
	g_data[0] = resultArr['TITLE']
	var rowcount = 1;
	for (var i in resultArr) {
		var indicator_row_arr = new Array();
		if (i != 'TITLE') {
			var indicator = resultArr[i];
			for ( var j in indicator ) {
				if(indicator[j] === undefined) {
					indicator[j] = '';
				}
				if (j == 0) {
					indicator_row_arr[j] = indicator[j];
				} else {
					indicator_row_arr[j] = parseFloat(indicator[j]);
				}
			}
		  g_data[rowcount] = indicator_row_arr;
			rowcount = rowcount + 1;
		}
	}
/* Parse the values to Google */	
result = g_data;
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
drawChart(result)
}
/* Draw google chart */
function drawChart(result){
  if (google.visualization != undefined) {
		var datas1 = google.visualization.arrayToDataTable(result);
	}
	var options1 = {
          title: indicatorText,     
          width: 700, 
          height: 500, 
          hAxis: {title: "Year"},
          vAxis: {title: "Indicator"},
          fontSize: 12,
          fontName: "Verdana"
  };
	var compooptions1 = {
          title: indicatorText,     
          width: 700, 
          height: 500, 
          hAxis: {title: "Year"},
          vAxis: {title: "Indicator"},
          seriesType: "bars",
          series: {1: {type: "line"}}
  };
  var tableoptions1 = {
          title: indicatorText,     
          showRowNumber: false
  };
  var chartType = $('#lstcharttype').val();
	if (chartType != 0){
    if (document.getElementById("BARs") && chartType == 1) {
      var chart = new google.visualization.BarChart(document.getElementById("BARs"));
      chart.draw(datas1, options1);
    	$('#BARs').show("slow");
     	$('#AREAs').slideUp();
     	$('#COLUMNs').slideUp();
     	$('#LINEs').slideUp();
     	$('#COMPOs').slideUp();
     	$('#PIEs').slideUp();
   	}
  	if (document.getElementById("AREAs") && chartType == 2) {
      var chart = new google.visualization.AreaChart(document.getElementById("AREAs"));
      chart.draw(datas1, options1);
     	$('#AREAs').show("slow");
     	$('#BARs').slideUp();
     	$('#COLUMNs').slideUp();
     	$('#LINEs').slideUp();
     	$('#COMPOs').slideUp();
     	$('#PIEs').slideUp();
   	}
   	if (document.getElementById("COLUMNs") && chartType == 3) {
    	var chart = new google.visualization.ColumnChart(document.getElementById("COLUMNs"));
    	chart.draw(datas1, options1);
     	$('#COLUMNs').show("slow");
     	$('#BARs').slideUp();
     	$('#AREAs').slideUp();
     	$('#LINEs').slideUp();
     	$('#COMPOs').slideUp();
    	$('#PIEs').slideUp();
  	}
    if (document.getElementById("LINEs") && chartType == 4) {
      var chart = new google.visualization.LineChart(document.getElementById("LINEs"));
	    chart.draw(datas1, options1);
	    $('#LINEs').show("slow");
	    $('#BARs').slideUp();
      $('#AREAs').slideUp();
      $('#COLUMNs').slideUp();
      $('#COMPOs').slideUp();
      $('#PIEs').slideUp();
    }
	  if (document.getElementById("COMPOs") && chartType == 5) {
	    var chart = new google.visualization.ComboChart(document.getElementById("COMPOs"));
	    chart.draw(datas1, compooptions1);
     	$('#COMPOs').show("slow");
      $('#BARs').slideUp();
     	$('#AREAs').slideUp();
     	$('#COLUMNs').slideUp();
     	$('#LINEs').slideUp();
     	$('#PIEs').slideUp();
  	}
    if (document.getElementById("PIEs") && chartType == 6) {
      var chart = new google.visualization.PieChart(document.getElementById("PIEs"));
    	chart.draw(datas1, options1);
     	$('#PIEs').show("slow");
      $('#BARs').slideUp();
    	$('#AREAs').slideUp();
     	$('#COLUMNs').slideUp();
     	$('#LINEs').slideUp();
     	$('#COMPOs').slideUp();
   	}
	}
	else if (chartType == 0){
    if (document.getElementById("PIEs")) {
	    var chart = new google.visualization.PieChart(document.getElementById("PIEs"));
      chart.draw(datas1, options1);
      $('#PIEs').show("slow");
    }
	  if (document.getElementById("COMPOs")) {
	    var chart = new google.visualization.ComboChart(document.getElementById("COMPOs"));
	    chart.draw(datas1, compooptions1);
      $('#COMPOs').show("slow");	        
    }
	  if (document.getElementById("LINEs")) {
	    var chart = new google.visualization.LineChart(document.getElementById("LINEs"));
	    chart.draw(datas1, options1);
     	$('#LINEs').show("slow");	        
    }
    if (document.getElementById("BARs")) {
      var chart = new google.visualization.BarChart(document.getElementById("BARs"));
      chart.draw(datas1, options1);
      $('#BARs').show("slow");          	
    }
    if (document.getElementById("COLUMNs")) {
     	var chart = new google.visualization.ColumnChart(document.getElementById("COLUMNs"));
     	chart.draw(datas1, options1);
     	$('#COLUMNs').show("slow");          	
    }
    if (document.getElementById("AREAs")) {
     	var chart = new google.visualization.AreaChart(document.getElementById("AREAs"));
     	chart.draw(datas1, options1);
     	$('#AREAs').show("slow");          	
    }
  }
}
});

